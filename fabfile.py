# -*- coding: utf-8 -*-
from __future__ import with_statement
from fabric.api import *


def setup_heroku():
    local('git init .')
    local('heroku apps:create {{ project_name }}')
    local('echo "web: gunicorn {{ project_name }}.wsgi --log-file -" >> Procfile')
    local('git add .')
    local('git commit -m "Setup {{ project_name }}"')
    local('git push heroku master')
    local('heroku addons:add heroku-postgresql')
    local('heroku run python manage.py syncdb')


def deploy_heroku():
    local('git push heroku master')


def deploy_heroku_migrate():
    local('git push heroku master')
    local('heroku run python manage.py syncdb --migrate')