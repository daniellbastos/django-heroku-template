Esse é um template base para novos projetos Django que serão hospedados no
Heroku.

** Requisitos **

- PostgreSQL

- Virtualenvwapper


** Como usar: **

Clone esse projeto em um diretório qualquer

mkvirtualenv meuprojeto

pip install django==1.6

django-admin.py startproject meuprojeto --template=/<path>/django-heroku-template --extension conf

cd meuprojeto

pip install -r requirements.txt

fab setup_heroku